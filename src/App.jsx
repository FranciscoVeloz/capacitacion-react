import React from "react";
import Layout from "./components/Layout";
import { BrowserRouter, Routes, Route } from "react-router-dom";

//Importing pages
import Task from "./pages/Task";
import Form from "./pages/Form";

//Importing styles
import "./styles/Styles.css";

const App = () => {
  return (
    <BrowserRouter>
      <Layout>
        <Routes>
          <Route path="/" element={<Task />} />
          <Route path="/add" element={<Form />} />
          <Route path="/edit/:id" element={<Form />} />
        </Routes>
      </Layout>
    </BrowserRouter>
  );
};

export default App;
