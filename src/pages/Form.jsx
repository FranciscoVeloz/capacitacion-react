import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { get, create, update } from "../services/task.services.js";
import "../styles/Form.css";

const Form = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  //Estados
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  //Obtenemos el task a editar
  const getTask = async () => {
    const result = await get(id);
    if (!result.status)
      return alert(`Something went wrong! - ${result.message}`);

    setTitle(result.data.title);
    setDescription(result.data.description);
  };

  //Al enviar la info
  const handleSubmit = async (e) => {
    e.preventDefault();

    const newTask = {
      title,
      description,
    };

    let result;

    if (id) {
      result = await update(id, newTask);
    } else {
      result = await create(newTask);
    }

    if (!result.status)
      return alert(`Something went wrong! - ${result.message}`);

    navigate(-1);
  };

  //Al cargar la app
  useEffect(() => {
    if (id) getTask();
  }, []);

  return (
    <div class="form-container">
      <h3 class="form-title">{id ? "Update" : "Save"} task</h3>

      <form class="form" onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder="Title"
          class="form-input"
          onChange={(e) => setTitle(e.target.value)}
          value={title}
        />

        <input
          type="text"
          placeholder="Description"
          class="form-input"
          onChange={(e) => setDescription(e.target.value)}
          value={description}
        />

        <button type="submit">{id ? "Update" : "Save"}</button>
      </form>
    </div>
  );
};

export default Form;
