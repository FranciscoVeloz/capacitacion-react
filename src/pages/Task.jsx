import React, { useState, useEffect } from "react";
import TaskItem from "../components/TaskItem";
import { list, deleteData } from "../services/task.services";
import "../styles/Task.css";

const Task = () => {
  const [tasks, setTasks] = useState([]);

  //Obtenemos las task
  const getTask = async () => {
    const result = await list();
    if (!result.status)
      return alert(`Something went wrong! - ${result.message}`);

    setTasks(result.data);
  };

  //Eliminamos una task
  const handleDelete = async (id, title) => {
    const res = confirm(`Do you want to delete the task "${title}"?`);
    if (!res) return;

    const result = await deleteData(id);
    if (!result.status)
      return alert(`Something went wrong! - ${result.message}`);

    getTask();
  };

  //Al cargar la app
  useEffect(() => {
    getTask();
  }, []);

  //Si no hay tasks
  if (tasks === undefined && tasks === null && tasks.length === 0)
    return <div>Loading...</div>;

  return (
    <div class="task-container">
      {tasks.map((t) => (
        <TaskItem
          id={t.id}
          title={t.title}
          description={t.description}
          hDelete={handleDelete}
        />
      ))}
    </div>
  );
};

export default Task;
