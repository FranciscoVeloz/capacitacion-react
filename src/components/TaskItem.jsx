import React from "react";
import { Link } from "react-router-dom";

const TaskItem = ({ id, title, description, hDelete }) => {
  return (
    <div class="task">
      <div class="task-header">{title}</div>
      <div class="task-body">
        <p>{description}</p>

        <div class="task-actions">
          <Link class="task-actions-btn" to={`/edit/${id}`}>
            Edit
          </Link>
          <button class="task-actions-btn" onClick={() => hDelete(id, title)}>
            Delete
          </button>
        </div>
      </div>
    </div>
  );
};

export default TaskItem;
