import React from "react";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <header>
      <Link to="/" class="title">
        JS App
      </Link>

      <Link to="/add">Add task</Link>
    </header>
  );
};

export default Navbar;
