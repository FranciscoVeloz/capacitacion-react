const API = "http://localhost:4000/api/tasks";

export const list = async () => {
  try {
    const response = await fetch(API);
    const data = await response.json();
    return data;
  } catch (error) {
    return { status: false, message: error.message };
  }
};

export const get = async (id) => {
  try {
    const response = await fetch(`${API}/${id}`);
    const data = await response.json();
    return data;
  } catch (error) {
    return { status: false, message: error.message };
  }
};

export const create = async (payload) => {
  try {
    const response = await fetch(API, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(payload),
    });
    const data = await response.json();
    return data;
  } catch (error) {
    return { status: false, message: error.message };
  }
};

export const update = async (id, payload) => {
  try {
    const response = await fetch(`${API}/${id}`, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(payload),
    });
    const data = await response.json();
    return data;
  } catch (error) {
    return { status: false, message: error.message };
  }
};

export const deleteData = async (id) => {
  try {
    const response = await fetch(`${API}/${id}`, {
      method: "DELETE",
    });
    const data = await response.json();
    return data;
  } catch (error) {
    return { status: false, message: error.message };
  }
};
